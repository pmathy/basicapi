# Welcome to "CX Software - Learning Community" Basic API Setup

The setup contained in this repository enables the user to create a Python Django
container setup. This setup presents an API to the user which allows for the creation
of "Device" objects via .json file POSTs to the /api/devices URL.

##### DISCLAIMER ####

This setup can NOT be regarded as production-ready, it is only meant for development purposes!
Making it more production secure is currently being worked on.

#### /DISCLAIMER ####

The purpose of this setup is to provide a quick way to get a Python Django
project started without hassling with python versions, virtual envs, databases,
frontend, etc.

What you get:
    - 3-tier setup with webserver, appserver & database
    - Django site accessible at http://localhost:8080
    - Django code accessible at ./django/data/*
    - Database data goes into ./db/data/
    - Operations script with functions to use while developing (e.g. create Django
    app, migrate database etc.)

Installation prerequisites:
    - Docker & Docker Compose installed
    - Python 3 installed (for Setup Script & Migration Scripts)
    - must be able to run Docker commands without sudo
    - must have the following information prepared for Installation Script:
        Database Name
        Database Username
        Database Password
        Django Secret Key (min. 16 digits)
        Django Superuser Username
        Django Superuser Password

# Must-Do to make the setup work:

Installation Script

    --> Run first-time setup
        python3 initialize.py

# Day-2 Operations Script (day2.py)

    your-pc$ python3 day2.py -help
    You can use the script with the following options:

    -help                   Help Menu
    -create [appname]       Create new Django App with name [appname]
    -shell                  Run Python Interactive Shell on Django Container
    -mkmig                  Make Django Database Migrations
    -migdb                  Migrate Django Database
    -migstat                Migrate Static content for Webserver
    -restart                Restart Django App server