import subprocess

#execute docker command in bash to catch Django App container ID
#subprocess.check_output calls bash and returns output
def getContainerID (container_name):
    container_id = subprocess.check_output("docker ps | grep " + container_name                                        + " | awk '{print $1}'", shell=True)
    container_id = container_id[0:-1].decode("utf-8")
    return container_id