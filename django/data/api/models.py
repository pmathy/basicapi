from django.db import models

class Device(models.Model):
    hostname = models.CharField(max_length=60, primary_key=True)
    deviceType = models.CharField(max_length=60)
    swVersion = models.CharField(max_length=60)

    def __str__(self):
        return self.hostname

class Connection(models.Model):
    name = models.CharField(max_length=60, primary_key=True)
    deviceA = models.OneToOneField(Device, on_delete=models.CASCADE, related_name='DeviceA')
    deviceB = models.OneToOneField(Device, on_delete=models.CASCADE, related_name='DeviceB')

    def __str__(self):
        return self.name