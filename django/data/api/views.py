from django.shortcuts import render

from rest_framework import viewsets, permissions

from .serializers import DeviceSerializer, ConnectionSerializer
from .models import Device, Connection


class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all().order_by('hostname')
    serializer_class = DeviceSerializer
    permission_classes = [permissions.IsAuthenticated]

class ConnectionViewSet(viewsets.ModelViewSet):
    queryset = Connection.objects.all().order_by('name')
    serializer_class = ConnectionSerializer
    permission_classes = [permissions.IsAuthenticated]