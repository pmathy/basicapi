# serializers.py
from rest_framework import serializers

from .models import Device, Connection

class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Device
        fields = ('hostname', 'deviceType', 'swVersion')

class ConnectionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Connection
        fields = ('name', 'deviceA', 'deviceB')