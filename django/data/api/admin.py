from django.contrib import admin
from .models import Device, Connection

# Register your models here.

admin.site.register(Device)
admin.site.register(Connection)