# views.py
from django.shortcuts import redirect

def redirect_root(request):
    response = redirect('/api/')
    return response